<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@store');

Route::get('/Jorge', function () {
    return ('JORGE123456789');
});

Route::get('/usuario', 'UserController@index');

Route::get('/usuarios/{num}', 'UserController@show');

Route::get('/usuario/{num}', 'UserController@show')->where('mun','[0-9]+');


Route::get('/project-l', function() {
	return view('plant');
});