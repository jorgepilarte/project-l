@extends('layout')

@section('title', "{$title}") @endsection

@section('content')

<h1>{{ $title }}</h1>

<ul>
	@forelse ($users as $user)
	<li>{{ $user }}</li>
	@empty
	<li>No hay user registrado.</li>
	@endforelse
</ul>


@endsection

@section('sidebar')

<h2>Barra laterial</h2>

@endsection

