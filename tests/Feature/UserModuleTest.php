<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModuleTest extends TestCase
{
    /**  @test */
    function para_cargar_lista_usuarios()
    {
        $response = $this->get('/Jorge');

        $response->assertStatus(200);
        $response->assertSee('JORGE123456789');
    }
}
