<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/usuario');

        $response->assertStatus(200);
        $response->assertSee('Juan');
        $response->assertSee('Oscar');
                 
    }
}
